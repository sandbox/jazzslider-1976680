<?php
/**
 * @file
 * commons_media_galleries.features.inc
 */

/**
 * Implements hook_views_api().
 */
function commons_media_galleries_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
