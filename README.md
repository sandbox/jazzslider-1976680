# Commons Media Galleries

This module provides integration between Drupal Commons 7.x-3.x and the
Media Gallery module (http://drupal.org/project/media_gallery).

DISCLAIMER: This module is not an official part of the Drupal Commons
ecosystem.  If you're running a Commons site and need to add Media
Galleries to your Groups, this is a great way to do it —but bear in mind
that any official solutions released by the Commons team later on may or
may not bear any similarities to this module.
