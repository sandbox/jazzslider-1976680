<?php
/**
 * @file
 * commons_media_galleries.features.og_features_permission.inc
 */

/**
 * Implements hook_og_features_default_permissions().
 */
function commons_media_galleries_og_features_default_permissions() {
  $permissions = array();

  // Exported og permission: 'node:group:create media_gallery content'
  $permissions['node:group:create media_gallery content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:group:delete any media_gallery content'
  $permissions['node:group:delete any media_gallery content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:group:delete own media_gallery content'
  $permissions['node:group:delete own media_gallery content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:group:update any media_gallery content'
  $permissions['node:group:update any media_gallery content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:group:update own media_gallery content'
  $permissions['node:group:update own media_gallery content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  return $permissions;
}
